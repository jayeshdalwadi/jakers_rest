class HomeController < ApplicationController
  def index
  end
  def sendFeedBack
    @data = false
    UserMailer.feed_back(params[:name],params[:email],params[:desc],params[:path]).deliver
    @data = true
    respond_to do |format|
      format.json { render :json =>  {:data => @data}}
    end
  end
end
