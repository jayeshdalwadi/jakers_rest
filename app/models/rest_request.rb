class RestRequest
  include Mongoid::Document
  include Mongoid::Timestamps


  field :user, type: String
  field :request_url, type: String
  field :request_parameters, type: String
  field :response_data, type: String
  field :response_header, type: String
  field :status, type: String
  def self.column_names
     self.fields
  end

end
