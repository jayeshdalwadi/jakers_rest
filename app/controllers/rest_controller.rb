class RestController < ApplicationController
  before_action :authenticate_user!
  def index
    @responseObject=nil
    @params_request=nil
    @request_header=nil
  end
  def request_process
    request_url = params[:url]
    method = params[:method]
    @params_request = params
    @request_header = {:User_agent =>request.user_agent, :Host =>request.remote_ip}

    @responseObject=nil
    if method == "get"
      @responseObject =  RestClient.get request_url
    elsif method== "post"
      @responseObject = RestClient.post request_url,params[:request_body],:content_type => params[:content_type]
    end
    @savedata = RestRequest.create(:user=> current_user.id,:request_url=>request_url,:request_parameters=>@params_request,:response_data=>@responseObject,:response_header=>"",:status=>@responseObject.code)
    render "index"
  end
end
