require File.expand_path('../boot', __FILE__)

require 'rails/all'
#require "action_controller/railtie"
#require "action_mailer/railtie"
#require "active_resource/railtie" # Comment this line for Rails 4.0+
#require "rails/test_unit/railtie"
# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(:default, Rails.env)


module JakersRest
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    # config.time_zone = 'Central Time (US & Canada)'

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    # config.i18n.default_locale = :de

    config.action_mailer.smtp_settings = {
        :address              => "smtp.1and1.com",
        :port                 => 25,
        :domain               => "dev.gridscape.com",
        :user_name            => "energyvault@grid-scape.com",
        :password             => "energy12345",
        :authentication       => "plain",
        :enable_starttls_auto => true
    }
    config.assets.enabled = true
    config.assets.initialize_on_precompile = false
    config.generators do |g|
      g.orm :mongoid
      g.orm :pg
    end
  end
end
