$(function(){
    $(window).scroll(function(){
        if ($(this).scrollTop()>600)
        {
            $('.header-info').slideUp(100);
            $('.header').addClass('trans');
        }
        else
        {
            $('.header-info').slideDown(100);
            $('.header').removeClass('trans');
        }
    });
});

/*
 * Style Switcher JS
 */

$(document).ready(function() {
    $('.style-btn').click(function (){
        var $box = $(this).parent('.style-switcher');
        if ($box.css('left') == '-31px'){
            $box.animate({ left: 0 }, 400);
        }
        else {
            $box.animate({ left: -31 }, 400);
        }
    });
});

/*
 * ScrollTo plugins
 */

jQuery(function( $ ){
    $('.scroll-link').click(function(){
        var section = $($(this).data("to"));
        if(section.offset()){
            var top = section.offset().top;
            $("html, body").animate({ scrollTop: top }, 1000);

            if($(window).width() < 768){
                var $navMain = $(".navbar-collapse");
                $navMain.collapse('hide');
            }
            return false;
        }

    });
});


/* *************************************** */
/* Parallax */
/* *************************************** */

$(document).ready(function(){

});

/* *************************************** */
/* Way Points JS */
/* *************************************** */

$(document).ready(function(){


});

/* ******************************************** */
/*  JS for Tool Tip  */
/* ******************************************** */

$('.social-tooltip').tooltip();

/* ******************************************** */
/* Owl-Carousel sponsor Slider */
/* ******************************************** */

$(document).ready(function() {
    $('#gmap').gmap3({
        map:{
            options:{
                center:[22.33365,73.18159],
                zoom:12,
                scrollwheel: false
            }
        }
    });
});

/* *************************************** */
/* Scroll to Top */
/* *************************************** */

$(".totop").hide();

$(function(){
    $(window).scroll(function(){
        if ($(this).scrollTop()>300)
        {
            $('.totop').fadeIn();
        }
        else
        {
            $('.totop').fadeOut();
        }
    });

    $('.totop a').click(function (e) {
        e.preventDefault();
        $('body,html').animate({scrollTop: 0}, 1500);
    });

});


$(function(){


$("#new_feedback_btn").click(function(){
    $("#feedback_name").val("");
    $("#feedback_email").val("");
    $("#feedback_desc").val("");
    $("#feed_back").modal("show");
});
});
/* ******************************************** */



