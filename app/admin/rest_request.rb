ActiveAdmin.register RestRequest do
  include Mongoid::Document
  include Mongoid::Timestamps

  config.per_page = 30

  permit_params :user, :request_url,:request_parameters,:response_data,:response_header,:status


  filter :user
  filter :request_url

  index do
    selectable_column
    column :user
    column :request_url
    column :request_parameters
    actions
  end

  show do
    attributes_table do
      row :user
      row :request_url
      row :response_data
      row :request_parameters
    end
  end

  form do |f|
    f.inputs "Post" do
      f.input :user
      f.input :request_url
    end
    f.actions
  end


end
