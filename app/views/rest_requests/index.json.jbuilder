json.array!(@rest_requests) do |rest_request|
  json.extract! rest_request, :id, :user, :request_url, :request_parameters, :response_data, :response_header, :status
  json.url rest_request_url(rest_request, format: :json)
end
