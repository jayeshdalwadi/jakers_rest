require 'savon'
class SoapController < ApplicationController
  before_action :authenticate_user!
  def index
    @responseObject=nil
    @params_request=nil
    @request_header=nil
  end
  def request_process
    @wsdl = params[:wsdl]
    @method = params[:method]
    @body_message = params[:body_message]
    #client = Savon.client(wsdl: "http://dev.grid-scape.com:8080/NEMA_Integration/services/StationDirectorySoapServerPort?wsdl")
    #client = Savon.client(wsdl:@wsdl)
    soap_header = {
            :Username  => "e2aa7d86ad0ef0f8a5daecf80bdd318c5078de525bfa7e2aa7d86ad",
            :Password  => "2c8b8920da0e9207171178a1d9160908",
            :digest => false
        #}
    }
    soap_header = {
        "wsse:Security" => {
            "@soapenv:mustUnderstand" => 1,
            "@soapenv:UsernameToken" =>{
            "Username"  => "e2aa7d86ad0ef0f8a5daecf80bdd318c5078de525bfa7e2aa7d86ad",
            "Password"  => "2c8b8920da0e9207171178a1d9160908"
            }
        }
    }
    #soap_header = {
    #    "wsse:Security" => {
    #        "wsse:UsernameToken" =>{
    #            "Username"  => "e2aa7d86ad0ef0f8a5daecf80bdd318c5078de525bfa7e2aa7d86ad",
    #            "Password"  => "2c8b8920da0e9207171178a1d9160908"
    #        }
    #    }
    #}

    client = Savon.client(wsdl: @wsdl, log: true,:ssl_verify_mode => :none, :ssl_cert_file => "#{Rails.root + "config/cacert.pem"}")
    #do
    #  wsse_signature Akami::WSSE::Signature.new(Akami::WSSE::Certs.new(:private_key_file  => File.expand_path(File.dirname(__FILE__)) + "/cacert.pem"))
    #end
    #http.auth.ssl.cert_key_file = File.expand_path(File.dirname(__FILE__)) + "/cacert.pem"
    #puts client.operations.to_json
    @operation = client.operations
    #@message = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:schemas.nema.org:evse:dir:xsd" xmlns:urn1="urn:schemas.nema.org:evse:cmn">   <soapenv:Header/>   <soapenv:Body><urn:StationSearchParameter><urn:SearchControl><urn:SearchRequestId>1</urn:SearchRequestId><urn:ResultsStartIndex>1</urn:ResultsStartIndex><urn:MaxResultsToReturn>1</urn:MaxResultsToReturn><urn:TotalResultCount>1</urn:TotalResultCount></urn:SearchControl><urn:AllStations>true</urn:AllStations></urn:StationSearchParameter></soapenv:Body></soapenv:Envelope>'
    @message = @body_message
    @data = client.operation(:"#{@method}")
    puts @data.build(:xml => @message, soap_header: soap_header)
    #client.wsse.credentials "e2aa7d86ad0ef0f8a5daecf80bdd318c5078de525bfa7e2aa7d86ad", "2c8b8920da0e9207171178a1d9160908"
    response = client.call(:"#{@method}",:xml => @message,:soap_header => soap_header)
    #do |locals|
    #  locals[:xml]= @message
    #  #locals[:soap_header] = false
    #  #locals.wsse_auth "e2aa7d86ad0ef0f8a5daecf80bdd318c5078de525bfa7e2aa7d86ad", "2c8b8920da0e9207171178a1d9160908"
    #end

    #puts client.request.headers.to_json
    @header =  response.http.headers
    #puts @header.to_json
    @params_request =  @message
    @responseObject = response.to_xml
    doc  = Nokogiri::XML(@responseObject)
    @doc =  doc
    render "index"
  end
  def get_methodname
    client = Savon.client(wsdl: "http://dev.grid-scape.com:8080/NEMA_Integration/services/StationDirectorySoapServerPort?wsdl")
    @operation = client.operations
  end
end
