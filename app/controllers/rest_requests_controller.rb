class RestRequestsController < ApplicationController
  before_action :set_rest_request, only: [:show, :edit, :update, :destroy]

  # GET /rest_requests
  # GET /rest_requests.json
  def index
    @rest_requests = RestRequest.all
  end

  # GET /rest_requests/1
  # GET /rest_requests/1.json
  def show
  end

  # GET /rest_requests/new
  def new
    @rest_request = RestRequest.new
  end

  # GET /rest_requests/1/edit
  def edit
  end

  # POST /rest_requests
  # POST /rest_requests.json
  def create
    @rest_request = RestRequest.new(rest_request_params)

    respond_to do |format|
      if @rest_request.save
        format.html { redirect_to @rest_request, notice: 'Rest request was successfully created.' }
        format.json { render action: 'show', status: :created, location: @rest_request }
      else
        format.html { render action: 'new' }
        format.json { render json: @rest_request.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /rest_requests/1
  # PATCH/PUT /rest_requests/1.json
  def update
    respond_to do |format|
      if @rest_request.update(rest_request_params)
        format.html { redirect_to @rest_request, notice: 'Rest request was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @rest_request.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /rest_requests/1
  # DELETE /rest_requests/1.json
  def destroy
    @rest_request.destroy
    respond_to do |format|
      format.html { redirect_to rest_requests_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_rest_request
      @rest_request = RestRequest.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def rest_request_params
      params.require(:rest_request).permit(:user, :request_url, :request_parameters, :response_data, :response_header, :status)
    end
end
