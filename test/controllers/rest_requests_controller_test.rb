require 'test_helper'

class RestRequestsControllerTest < ActionController::TestCase
  setup do
    @rest_request = rest_requests(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:rest_requests)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create rest_request" do
    assert_difference('RestRequest.count') do
      post :create, rest_request: { request_parameters: @rest_request.request_parameters, request_url: @rest_request.request_url, response_data: @rest_request.response_data, response_header: @rest_request.response_header, status: @rest_request.status, user: @rest_request.user }
    end

    assert_redirected_to rest_request_path(assigns(:rest_request))
  end

  test "should show rest_request" do
    get :show, id: @rest_request
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @rest_request
    assert_response :success
  end

  test "should update rest_request" do
    patch :update, id: @rest_request, rest_request: { request_parameters: @rest_request.request_parameters, request_url: @rest_request.request_url, response_data: @rest_request.response_data, response_header: @rest_request.response_header, status: @rest_request.status, user: @rest_request.user }
    assert_redirected_to rest_request_path(assigns(:rest_request))
  end

  test "should destroy rest_request" do
    assert_difference('RestRequest.count', -1) do
      delete :destroy, id: @rest_request
    end

    assert_redirected_to rest_requests_path
  end
end
